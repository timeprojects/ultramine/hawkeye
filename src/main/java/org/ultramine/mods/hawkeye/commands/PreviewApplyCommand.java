package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.Rollback;
import org.ultramine.mods.hawkeye.Rollback.RollbackType;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Applies a local rollback to the world Error handling for user input is done
 * using exceptions to keep code neat.
 * 
 * @author oliverw92
 */
public class PreviewApplyCommand extends BaseCommand
{
	public PreviewApplyCommand()
	{
		name = "preview apply";
		argLength = 0;
		usage = "<- apply rollback preview";
	}

	@Override
	public boolean execute()
	{

		//Check if player already has a rollback processing
		if(!session.isInPreview())
		{
			HawkUtil.sendMessage(sender, "&cNo preview to apply!");
			return true;
		}

		//Undo local changes to the player
		HawkUtil.sendMessage(sender, "&cAttempting to apply rollback to world...");
		new Rollback(RollbackType.GLOBAL, session);
		session.setInPreview(false);
		return true;

	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cApplies the results of a &7/hawk preview&c globally");
		HawkUtil.sendMessage(sender, "&cUntil this command is called, the preview is only visible to you");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.preview(sender);
	}
}
