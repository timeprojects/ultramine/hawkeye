package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.ToolManager;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

public class ToolResetCommand extends BaseCommand
{
	public ToolResetCommand()
	{
		name = "tool reset";
		argLength = 0;
		usage = " <- resets tool to default properties";
	}

	@Override
	public boolean execute()
	{
		ToolManager.resetTool(session);
		HawkUtil.sendMessage(player, "&cTool reset to default parameters");
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cReset HawkEye tool to default properties");
		HawkUtil.sendMessage(sender, "&cSee &7/hawk tool bind help");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.toolBind(sender);
	}
}