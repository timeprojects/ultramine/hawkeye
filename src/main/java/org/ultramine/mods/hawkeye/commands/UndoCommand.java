package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.Undo;
import org.ultramine.mods.hawkeye.Rollback.RollbackType;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Reverses the previous {@link RollbackCommand}
 * 
 * @author oliverw92
 */
public class UndoCommand extends BaseCommand
{
	public UndoCommand()
	{
		name = "undo";
		usage = "<- reverses your previous rollback";
	}

	@Override
	public boolean execute()
	{
		new Undo(session.isInPreview() ? RollbackType.LOCAL : RollbackType.GLOBAL, session);
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cReverses your previous rollback if you made a mistake with it");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.rollback(sender);
	}
}
