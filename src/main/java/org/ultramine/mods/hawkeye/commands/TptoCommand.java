package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.mods.hawkeye.database.DataManager;
import org.ultramine.mods.hawkeye.entry.DataEntry;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;
import org.ultramine.server.Teleporter;

import net.minecraft.world.World;

/**
 * Teleports player to location of specified data entry
 * 
 * @author oliverw92
 */
public class TptoCommand extends BaseCommand
{
	public TptoCommand()
	{
		name = "tpto";
		argLength = 1;
		usage = "<id> <- teleport to location of the data entry";
	}

	@Override
	public boolean execute()
	{
		if(!HawkUtil.isInteger(args.get(0)))
		{
			HawkUtil.sendMessage(sender, "&cPlease supply a entry id!");
			return true;
		}
		DataEntry entry = DataManager.getEntry(Integer.parseInt(args.get(0)));
		if(entry == null)
		{
			HawkUtil.sendMessage(sender, "&cEntry not found");
			return true;
		}
		World world = HawkEye.getWorld(entry.getWorld());
		if(world == null)
		{
			HawkUtil.sendMessage(sender, "&cWorld &7" + entry.getWorld() + "&c does not exist!");
			return true;
		}

		Teleporter.tpNow(player, world.provider.dimensionId, entry.getX(), entry.getY(), entry.getZ());

		HawkUtil.sendMessage(sender, "&7Teleported to location of data entry &c" + args.get(0));
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cTakes you to the location of the data entry with the specified ID");
		HawkUtil.sendMessage(sender, "&cThe ID can be found in either the DataLog interface or when you do a search command");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.tpTo(sender);
	}
}