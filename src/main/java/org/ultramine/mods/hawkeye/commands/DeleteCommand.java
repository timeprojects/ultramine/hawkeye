package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.SearchParser;
import org.ultramine.mods.hawkeye.callbacks.DeleteCallback;
import org.ultramine.mods.hawkeye.database.SearchQuery;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchDir;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

public class DeleteCommand extends BaseCommand
{
	public DeleteCommand()
	{
		bePlayer = false;
		name = "delete";
		argLength = 1;
		usage = "<parameters> <- delete database entries";
	}

	@Override
	public boolean execute()
	{
		//Parse arguments
		SearchParser parser = null;
		try
		{
			parser = new SearchParser(sender, args);
		}
		catch(IllegalArgumentException e)
		{
			HawkUtil.sendMessage(sender, "&c" + e.getMessage());
			return true;
		}

		//Create new SeachQuery with data
		new SearchQuery(new DeleteCallback(session), parser, SearchDir.DESC);
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cDeletes specified entries from the database permanently");
		HawkUtil.sendMessage(sender, "&cUses the same parameters and format as /hawk search");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.delete(sender);
	}
}
