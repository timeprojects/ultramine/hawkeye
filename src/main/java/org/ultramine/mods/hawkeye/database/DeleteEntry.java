package org.ultramine.mods.hawkeye.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.mods.hawkeye.entry.DataEntry;
import org.ultramine.mods.hawkeye.util.HawkUtil;

public class DeleteEntry implements Runnable
{
	private final List<Long> ids = new ArrayList<Long>();

	public DeleteEntry(Long id)
	{
		ids.add(id);
	}

	public DeleteEntry(DataEntry entry)
	{
		ids.add(entry.getDataId());
	}

	public DeleteEntry(List<?> entries)
	{
		for(int i = 0; i < entries.size(); i++)
		{
			if(entries.get(i) instanceof DataEntry)
				ids.add(((DataEntry) (entries.get(i))).getDataId());
			else
				ids.add((Long) entries.get(i));
		}
	}

	public void run()
	{
		JDCConnection conn = null;
		try
		{
			conn = DataManager.getConnection();
			for(Long id : ids)
			{
				conn.createStatement().executeUpdate("DELETE FROM `" + HawkEye.instance.config.database.dataTable + "` WHERE `data_id` = " + id);
			}
		}
		catch(SQLException ex)
		{
			HawkUtil.error("Unable to delete data entries from MySQL database: ", ex);
		}
		finally
		{
			conn.close();
		}
	}
}