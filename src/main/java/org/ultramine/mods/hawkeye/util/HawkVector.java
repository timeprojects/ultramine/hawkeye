package org.ultramine.mods.hawkeye.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class HawkVector
{
	protected double x;
	protected double y;
	protected double z;

	public HawkVector()
	{

	}

	public HawkVector(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public HawkVector(EntityPlayer player)
	{
		this.x = player.posX;
		this.y = player.posY;
		this.z = player.posZ;
	}

	public double getX()
	{
		return x;
	}

	/**
	 * Gets the floored value of the X component, indicating the block that this
	 * vector is contained with.
	 *
	 * @return block X
	 */
	public int getBlockX()
	{
		return MathHelper.floor_double(x);
	}

	/**
	 * Gets the Y component.
	 *
	 * @return The Y component.
	 */
	public double getY()
	{
		return y;
	}

	/**
	 * Gets the floored value of the Y component, indicating the block that this
	 * vector is contained with.
	 *
	 * @return block y
	 */
	public int getBlockY()
	{
		return MathHelper.floor_double(y);
	}

	/**
	 * Gets the Z component.
	 *
	 * @return The Z component.
	 */
	public double getZ()
	{
		return z;
	}

	/**
	 * Gets the floored value of the Z component, indicating the block that this
	 * vector is contained with.
	 *
	 * @return block z
	 */
	public int getBlockZ()
	{
		return MathHelper.floor_double(z);
	}

	/**
	 * Set the X component.
	 *
	 * @param x
	 *            The new X component.
	 * @return This vector.
	 */
	public HawkVector setX(int x)
	{
		this.x = x;
		return this;
	}

	/**
	 * Set the X component.
	 *
	 * @param x
	 *            The new X component.
	 * @return This vector.
	 */
	public HawkVector setX(double x)
	{
		this.x = x;
		return this;
	}

	/**
	 * Set the X component.
	 *
	 * @param x
	 *            The new X component.
	 * @return This vector.
	 */
	public HawkVector setX(float x)
	{
		this.x = x;
		return this;
	}

	/**
	 * Set the Y component.
	 *
	 * @param y
	 *            The new Y component.
	 * @return This vector.
	 */
	public HawkVector setY(int y)
	{
		this.y = y;
		return this;
	}

	/**
	 * Set the Y component.
	 *
	 * @param y
	 *            The new Y component.
	 * @return This vector.
	 */
	public HawkVector setY(double y)
	{
		this.y = y;
		return this;
	}

	/**
	 * Set the Y component.
	 *
	 * @param y
	 *            The new Y component.
	 * @return This vector.
	 */
	public HawkVector setY(float y)
	{
		this.y = y;
		return this;
	}

	/**
	 * Set the Z component.
	 *
	 * @param z
	 *            The new Z component.
	 * @return This vector.
	 */
	public HawkVector setZ(int z)
	{
		this.z = z;
		return this;
	}

	/**
	 * Set the Z component.
	 *
	 * @param z
	 *            The new Z component.
	 * @return This vector.
	 */
	public HawkVector setZ(double z)
	{
		this.z = z;
		return this;
	}

	/**
	 * Set the Z component.
	 *
	 * @param z
	 *            The new Z component.
	 * @return This vector.
	 */
	public HawkVector setZ(float z)
	{
		this.z = z;
		return this;
	}
}
