package org.ultramine.mods.hawkeye.callbacks;

import net.minecraft.command.ICommandSender;

import org.ultramine.mods.hawkeye.PlayerSession;
import org.ultramine.mods.hawkeye.Rollback;
import org.ultramine.mods.hawkeye.Rollback.RollbackType;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchError;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Implementation of BaseCallback for use in rollback commands
 * 
 * @author oliverw92
 */
public class RollbackCallback extends BaseCallback
{
	private final PlayerSession session;
	private final ICommandSender sender;
	private final RollbackType type;

	public RollbackCallback(PlayerSession session, RollbackType type)
	{
		this.type = type;
		this.session = session;
		sender = session.getSender();
		HawkUtil.sendMessage(sender, "&cSearching for matching results to rollback...");
	}

	@Override
	public void execute()
	{
		session.setRollbackResults(results);
		new Rollback(type, session);
	}

	@Override
	public void error(SearchError error, String message)
	{
		HawkUtil.sendMessage(session.getSender(), message);
	}
}
